module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      white: {
        default: '#FFFFFF',
        rose: '#fffefe',
        smoke: '#f5f5f5',
        silver: '#a1a1a1'
      },
      black: {
        chinese: '#101010'
      }
    },
    extend: {
      spacing: {
        '12-5': '3.125rem',
        '13-5': '3.375rem',
        18: '4.5rem',
        '20-5': '5.125rem',
        45: '11.25rem',
        60: '15rem',
        '60-5': '15.125rem',
        '61-25': '15.3125rem',
        66: '16.5rem',
        '107-5': '26.875rem',
        '107-75': '26.9375rem',
        '112-25': '28.0625rem',
        '112-5': '28.125rem',
        123: '30.75rem',
        '1-45': 'calc(100vh - 11.25rem)',
        '1+2': 'calc(100vh + 2rem)'
      },
      fontSize: {
        '2x1-1': '1.625rem',
        '3x1-2-125': '2.125rem',
        '4x1-1': '2.5rem'
      },
      lineHeight: {
        9: '2.25rem',
        10: '2.5rem'
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
